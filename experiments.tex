\section{Experiments and Results}

We evaluate the three models on three tasks:
entity prediction, relation prediction, and temporal scope prediction.
Results are reported in this section.

\subsection{Datasets}

We employ two temporal knowledge graph datasets for evaluation: YAGO11k and Wikidata12k,
both are proposed to evaluate time-aware representations\cite{HyTE}.
The dataset statistics in detail are shown in Table \ref{statistics}.

\begin{itemize}
\item \textbf{YAGO11k}: YAGO11k is extracted from YAGO3 \cite{yago},
it contains 20.5k facts, with start time and end time attached to each entity-relation triple.
\item \textbf{Wikidata12k}: Wikidata12k is a subset of Wikidata \cite{wikidata},
which is extracted in the same way as YAGO11k.
It has a much bigger size than YAGO11k, with 40k facts and 12.5k entities.
\end{itemize}

\subsection{Set of Timestamps}\label{three}
Since there are a huge number of timestamps in a given knowledge graph,
treating each of them uniquely will cause additional complexity to the model.
We redefine a set of new timestamps for the given knowledge graph, and deal
with this new set in training and evaluation.
In general, the number of facts per original time interval is not balanced,
there may be hundreds of facts in one year,
while only few facts in other ten years.
Thus, we define a threshold of the time interval to balance the number of facts in different time intervals.
Original intervals with a small number of facts are merged into one larger interval,
and the intervals with lots of facts are self-contained.
This procedure is the same as in HyTE\cite{HyTE}.
To simplify the symbols used, we still denote the refined set of timestamps as
$\mathcal{T}=\{\tau_1, \tau_2, \cdots, \tau_N\}$.

\input{tableRelation}
\input{tableTime}

\subsection{Experimental Settings}

\subsubsection{Baselines}
We compare the three models against five approaches,
including both static representation learning methods and time-aware methods.
\begin{itemize}
\item \textbf{Static representation learning baselines} \\
We use three embedding learning methods for static knowledge graph,
including TransE\cite{TransE}, TransH\cite{TransH}, and HolE\cite{HolE}.
TransE and TransH are both simple but effective translational methods.
HolE is used as the current state-of-the-art model for static baselines.
For these models,
we simply ignore the temporal information and treat them as $(h,r,t)$ triples.
%during both training and evaluation phases.

\item \textbf{Time-aware representation learning baselines} \\
For time-aware models,
we use t-TransE\cite{tTransE} and HyTE\cite{HyTE} as baselines.
t-TransE models temporal information based on ordering of relations.
HyTE projects entities and relations onto a time-specific hyperplane.
HyTE is used as the state-of-the-art model for time-aware baselines.
As we use the same methods and datasets as in HyTE\cite{HyTE},
we borrow the experimental results directly from the HyTE paper. 
\end{itemize}


\subsubsection{Implementation Details}
We set the batch size as $500$, set the dimensions of entity, relation, and time space equal to each other,
we use 20 negative samples in negative sampling.
We choose the dimension among $\{64, 128, 256\}$,
margin from $\{1, 5, 10\}$.
%optimizer from SGD, Adam, and RMSProp.
The learning rate is chosen from $\{0.00001, 0.0001, 0.001, 0.01\}$,
and the threshold of time interval sample number is chosen from $\{100, 300, 500\}$.

We set the value of parameters based on the experimental results on validation set,
and choose parameters for each model respectively.
Optimal parameters for the three models are:
dimension $k_r = k_e = k_t = 128$,
margin $\gamma = 10$,
and the threshold equals $300$.
We use optimizer Adam.
Learning rate equals $0.0001$ while training \rel{},
and $0.00001$ while training \ent{} and \both{}.
We limit the training procedure in $50$ epochs.
Note that the results we reported in different experiments are get by the same parameter setting.

\subsection{Entity Prediction}

Aimed at predicting missing entities in triples,
entity prediction has been widely adopted since being proposed in \cite{linkprediction}.

\subsubsection{Evaluation Protocal}
We follow the protocol proposed in \cite{linkprediction},
first we construct negative samples with replacing head entity by every other entities in $\mathcal{E}$,
then rank all these triples based on their scores.
Rank of tail entities can get in the same way except replacing tail entities
instead of head entities.

We use two metrics: mean rank and Hits@10 in the evaluation phase.
Mean rank is the average rank for correct entities,
and hits@10 is the proportion of correct entities ranked within top 10.
A more capable model requires a lower mean rank and a higher Hits@10.

\subsubsection{Evaluation Results}
Evaluation results on YAGO11k and Wikidata12k are shown in Table \ref{entity}.
Model \rel\ gets the best results with significant improvements according to the
experiments,
thus it can encode temporal information into embeddings effectively.

However, \ent\ and \both\ cannot get such outstanding results.
Results of \ent\ and \both\ are better than static models in most cases,
but worse than HyTE.
This indicates that \ent\ and \both\ can learn time information,
but cannot use it as well as model \rel\ and HyTE.
From these experiments, we find that temporal information may have less influence on entities than on relations.
We will discuss this further in Section \ref{five}.

\subsection{Relation Prediction}

Relation prediction is similar with entity prediction,
aiming at predicting relations between entities.

\subsubsection{Evaluation Protocal}

We construct negative samples by replacing relations with every other relation in relation set $\mathcal{R}$,
and rank these triples based on their scores.

We report two metrics for this task: mean rank and Hits@1.
Mean rank is the average rank for correct relations,
Hits@1 is the proportion of correct relations ranked top 1.
In this task,
we want a lower mean rank and a higher Hits@1.

\subsubsection{Evaluation Results}
Evaluation results are shown in Table \ref{relation}.
Model \rel\ gets the best results among these models,
while results of \ent\ and \both\ are not so well.
As results of \ent\ on this task are the worst in the three models,
and \ent\ is the only model which does not consider the effects of time on relation.
This experiment shows that effects of temporal information on relation are valuable.
This will be discussed further in Section \ref{five}.

\input{tableAnalysis}

\input{tableComplexity}
 
\subsection{Temporal Scope Prediction}

Proposed in paper \cite{HyTE},
temporal scope prediction is a task aiming to predict in which time interval does the fact hold.

\subsubsection{Evaluation Protocal}
We follow the protocol described in \cite{HyTE}.
Intervals are set based on the threshold of instance numbers per interval (see detail in Section \ref{three}),
we use the optimal threshold $300$.
For all models, YAGO11k and Wikidata12k are treated as $61$ and $78$ intervals respectively.
We replace time interval with every other possible interval
and rank them based on their scores.
A lower rank is preferred in this task.

As static methods do not take time into account,
t-TransE \cite{tTransE} does not explicitly encode temporal information into embeddings,
thus they cannot perform this task.
Only HyTE is used as baseline in this task.

\subsubsection{Evaluation Results}
Temporal scope prediction results are shown in Table \ref{time}.
Model \rel\ outperforms baseline HyTE,
while model \ent\ and \both\ get slightly worse results than HyTE.
This shows that \rel\ can encode temporal information more efficiently,
which is consistent with the results of the first two tasks.
