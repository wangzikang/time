\section{Methodology}

This section describes three models we proposed to learn time-aware
representation for knowledge graph. The training method and how to construct
training sets are also explained.

\subsection{Problem Definition and Notations}
We denote a \textit{temporal knowledge graph} as $\mathcal{G}=\{(h,r,t, \tau_s,
\tau_e)\}\subseteq \mathcal{E} \times \mathcal{R} \times \mathcal{E} \times \mathcal{T} \times \mathcal{T}$,
where $\mathcal{E}$ denotes the set of entities, $\mathcal{R}$ denotes the set of relations and $\mathcal{T}$ denotes the set of timestamps,
$h,t \in \mathcal{E}$, $r\in \mathcal{R}$, $\tau_s, \tau_e \in \mathcal{T}$. 
A tuple $(h,r,t,\tau_s, \tau_e)$ represents a time-aware fact which says head entity $h$ and tail entity
$t$ have relation $r$ during the time scope from $\tau_s$ to $\tau_e$.
Given a time-aware fact $(h,r,t,\tau_s, \tau_e)$,
we can equivalently describe it as a set of quadruples $\{(h,r,t,\tau_{k_1}),
(h,r,t,\tau_{k_2}), \cdots, (h,r,t,\tau_{k_n})\}$,
if we let $(h,r,t,\tau_{k_i})$ denote head entity $h$ and tail entity
$t$ have relation $r$ at the timestamp $\tau_{k_i}$, 
where $\tau_{k_1}, \tau_{k_2}, \cdots, \tau_{k_n}$ are timestamps included in time scope $\tau_s$ to
$\tau_e$. In the following, we will adopt quadruple $(h,r,t,\tau)$ to
formulate our models.

In most representation learning methods or approaches, all entities and
relations are considered lay in the
same space. However, in this paper, we differentiate the entity space and the
relation space to extend the expression power of the proposed models 
by allowing them be affected by different transformation rules.

For each fact $(h,r,t)$ in a given knowledge graph, 
the \textit{time-aware representation learning} aims to learn time-specific
embeddings $\bm{h}_{\tau_k}$ and $\bm{t}_{\tau_k}$for entities $h$ and
$t$, and $\bm{r}_{\tau_k}$ for relation $r$ when given a timestamp $\tau_k\in\mathcal{T}$, where $k=1, 2, \cdots, N$,
$N\in\mathbb{N}$ is a nature number.
In general, all entities and relations can evolve with time elapse.
Therefore, it is not affordable to track all the entity and relation vectors at each
timestamp. To address this challenge, we assume that for each entity and each
relation, these is a constant embedding vector which is employed to represent it.
We denote $\bm{h}, \bm{t} \in \mathbb{R}^{k_e}$ as the constant embeddings of entities $h$ and $t$,
$\bm{r} \in \mathbb{R}^{k_r}$ as the constant embedding of relation $r$,
where $k_e$ is the dimension of the entity space $\mathbb{R}^{k_e}$, and $k_r$ is the dimension of
the relation space $\mathbb{R}^{k_r}$.
For each timestamp $\tau_k$, to obtain the time-specific embeddings, 
we will learn a time-specific transformation to convert the constant embeddings,
$\bm{h}, \bm{t}, \bm{r}$, as the time-specific embeddings, $\bm{h}_{\tau_k},
\bm{t}_{\tau_k}, \bm{r}_{\tau_k}$. 

\subsection{Models}

A direct model according to the above description is considering both
entity and relation are time-sensitive, therefore we call this the \both\ (i.e.,
Both Time-Sensitive) model. Obviously, there are two simplifications towards the
\both{} model, one assumes only entities are time-sensitive and the other one consider
only relations are time-sensitive. We call the former the \ent\ (i.e., Entity
Time-Sensitive) model and the latter the \rel\ (i.e., Relation Time-Sensitive) model.

\subsubsection{Model I: \both}
\both\ assumes that both entities and relations are affected by time.
At a specific timestamp $\tau_k$,
we project the entity and relation embeddings into a new time space respectively.
This model is illustrated in Fig \ref{both}.

\input{figBoth}

At time $\tau_k$,
denote $M_{e\tau_k} \in \mathbb{R}^{k_e \times k_t}$ as the projection matrix
from the entity space to the time space,
and $M_{r\tau_k} \in \mathbb{R}^{k_r \times k_t}$ as the projection matrix from
the relation space to the time space,
where $k_t \in \mathbb{R}$ is the dimension of time space.
Then time specific embeddings of entities and relations at timestamp $\tau_k$
can be obtained by the following projection transformation (all embeddings are
represented by row vectors)
\begin{align}
    \bm{h}_{\tau_k} &= \bm{h}M_{e \tau_k} , \\
    \bm{r}_{\tau_k} &= \bm{r}M_{r \tau_k} , \\
    \bm{t}_{\tau_k} &= \bm{t}M_{e \tau_k} .
\end{align}
Note that, when all time spaces degenerated as hyperplanes, the proposed \both\
model is just the same as HyTE.


\subsubsection{Model II: \ent}
\ent\ is under the assumption that entities and relations belong to different spaces
and only the entity space is affected by time.
At a specific timestamp,
we project entity embeddings into the relation space based on a time-specific transformation matrix.
Model illustration is shown in Fig \ref{ent}.

\input{figEnt}

Denote $M_{\tau_k} \in \mathbb{R}^{k_e \times k_r}$ as the projection matrix at time $\tau_k$,
which projects embeddings from the entity space to the relation space.
The time-specific entity embeddings at time $\tau_k$ can be computed by the
follow project transformation
\begin{align}
    \bm{h}_{\tau_k} &= \bm{h}M_{\tau_k} , \\
    \bm{t}_{\tau_k} &= \bm{t}M_{\tau_k} .
\end{align}
Since relation embedding is not affected by time, it equals to its corresponding
constant embedding
\begin{align}
    \bm{r}_{\tau_k} = \bm{r}
\end{align}

\subsubsection{Model III: \rel}
In the \rel\ model, we still put entities and relations in two different spaces,
while only relation space is affected by time.
At a specific time $\tau_k, k \in [1,N]$,
we project relation embeddings into entity space based on time-specific transformation matrix.
The basic idea is illustrated in Fig \ref{rel}.

\input{figRel}

Denote $M_{\tau_k} \in \mathbb{R}^{k_r \times k_e}$ as the projection matrix at time $\tau_k$,
which projects embeddings from the relation space to the entity space.
Then the relation embedding at time $\tau_k$ is
\begin{align}
    \bm{r}_{\tau_k} = \bm{r}M_{\tau_k} .
\end{align}
Embeddings of entities $h$ and $t$ always stay the same as they do not affect by
time
\begin{align}
    \bm{h}_{\tau_k} &= \bm{h} , \\
    \bm{t}_{\tau_k} &= \bm{t} .
\end{align}

\subsection{Model Training}
For all three models constructed above,
we want the projected embeddings in a specific time interval $\tau_k$ satisfy the TransE hypothesis,
i.e.,
relation embeddings can be seen as translation vectors from head entities to
tail entities
\begin{align}
    \bm{h}_{\tau_k} + \bm{r}_{\tau_k} \approx \bm{t}_{\tau_k},
\end{align}
which has been proved simple and effective in various translational representation learning models.

To achieve this goal,
we define a score function 
$\varphi: \mathcal{E} \times \mathcal{R} \times \mathcal{E} \times \mathcal{T} \to \mathbb{R}$
for quadruple $(h,r,t, \tau)$.
The score is calculated based on the learned time-specific embeddings
$\bm{h}_{\tau_k}$, $\bm{r}_{\tau_k}$, and $\bm{t}_{\tau_k}$.
For a given quadruple,
its score is proportional to the likelihood of it being true.
We want the score is lower for correct quadruples in the given knowledge graph
and higher for false quadruples not in the given knowledge graph.
The score function is the same for all three models
\begin{align}
    f_{\tau_k}(h,r,t) = \| \bm{h}_{\tau_k} + \bm{r}_{\tau_k}- \bm{t}_{\tau_k} \| .
\end{align}
Note that the embeddings $\bm{h}_{\tau_k}, \bm{r}_{\tau_k}$, and $\bm{t}_{\tau_k}$ are normalized before calculating scores.

We train our model by minimizing a margin-based ranking loss over the training set
\begin{align}
L = \sum_{(h,r,t)\in \mathcal{S}} \sum_{(h',r',t')\in \mathcal{S'}} \max(&0, [\gamma + f_{\tau_k}(h,r,t) - \\ \notag
& f_{\tau_k}(h',r',t')]) .
\end{align}
where $\gamma$ is the margin, $\max(0,x)$ returns the positive part of $x$,
$\mathcal{S}$ is the set of correct quadruples in the given knowledge graph,
and $\mathcal{S^\prime}$ is the set of false quadruples constructed by negative sampling.

\input{tableEntity}
\input{tableStatistics}

There are two ways to construct $S^\prime$.
%One widely used way in existing knowledge graph representation learning
One widely used way is
\begin{align}
	\mathcal{S}_{h,r,t,\tau}^{'} = &\{(h', r, t, \tau)|h' \in \mathcal{E}, (h',r,t) \notin \mathcal{S} \} \cup \\ \notag 
	&\{(h,r,t',\tau)|t' \in \mathcal{E}, (h,r,t') \notin \mathcal{S}\}.
\end{align}
It ignores temporal information and selects triples that does not exist in the whole knowledge graph.
We use this negative sampling technique in both entity prediction and relation
prediction tasks to evaluate the learned embeddings.

For time scope prediction task,
we use time dependent negative sampling (TDNS, \cite{HyTE}).
For a given quadruple $(h,r,t,\tau)$,
besides the negative samples obtained by the above method,
it also samples negative triples that exist in knowledge graph at times other than $\tau$.
We get negative samples in this scenario following

\begin{align}
    \mathcal{S}_{h,r,t,\tau}^{'} = &\{(h', r, t, \tau)|h' \in \mathcal{E}, (h',r,t) \in \mathcal{S}, \\ \notag
    & (h',r,t,\tau) \notin \mathcal{S}_{\tau} \} \cup \{(h,r,t',\tau)|t' \in \mathcal{E}, \\ \notag
    & (h,r,t') \in \mathcal{S}, (h,r,t',\tau) \notin \mathcal{S}_{\tau} \} .
\end{align} 

We train our model using gradient descent over training set,
and choose optimal parameters on validation set.
